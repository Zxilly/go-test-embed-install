package main

import (
	_ "embed"
	"fmt"
)

//go:embed .gitlab-ci.yml
var gitlabCiYml string

func main() {
	fmt.Println(gitlabCiYml)
}